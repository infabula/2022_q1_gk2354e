import os.path


def ask_names(filename):
   with open(filename, 'w') as outfile:
       while True:
           raw_name = input("Give a name: ")
           name = raw_name.strip()
           if not name:
               return
           else:
               outfile.write(f"{name}\n")  # include newline


def print_names_from_file(filename):
    with open(filename, 'r') as infile:
        for line in infile:
            print(line, end='')


def main():
    try:
        filename = os.path.join("data", 'names.txt')
        c
        ask_names(filename)
        print_names_from_file(filename)
    except Exception as e:
        print("Something went wrong...")
        print(e)

if __name__ == "__main__":
    main()