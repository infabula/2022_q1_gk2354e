def ask_amount():
    amount = int(input("How many? "))
    if amount < 0:
        the_exception = ValueError("Amount can not be negative.")
        the_exception.amount = amount
        raise the_exception
    else:
        return amount


def main():
    amount = None
    try:
        amount = ask_amount()
        message = "The user wants " + str(amount)
        print(message)
    except ValueError as e:
        print(f"{e.amount} is not a valid value.")
        amount = 0



if __name__ == "__main__":
    main()