def main():
    admin_hour_rate = 84.0

    server_price = input("What is price per month for one VPS?")
    server_price = float(server_price)

    nb_of_servers = int(input("How many VPS servers do you need?"))
    admin_time_per_vps = int(input("How many minutes needs an administrator to provision one VPS?"))

    total_admin_minutes = nb_of_servers * admin_time_per_vps
    admin_costs_per_minute = admin_hour_rate / 60

    admin_costs = admin_costs_per_minute * total_admin_minutes
    server_costs = server_price * nb_of_servers

    total_price = server_costs + admin_costs
    rounded = round(total_price, 2)

    msg = "The total cost for " + str(nb_of_servers) + " servers is " + str(rounded) + " euro."
    print(msg)



main()



