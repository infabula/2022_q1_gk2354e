"""


"""

def print_dns(dns):
    """Prints the dns in a nice table format.
    :arg the dns dictionary
    """
    #print("Printing the dns")
    header = "{hostname_header:^22}{ip_header:^20}".format(hostname_header="HOSTNAME",
                                                             ip_header="IP ADDRESS")
    print(header)
    for hostname, ip in dns.items():
        line = f"- {hostname:<20}{ip:>20}"
        print(line)
    return dns

def ask_hostname():
    """TODO doc"""
    #print("Aksing the user for hosname.")
    hostname = input("Hostname:")
    # TODO : sanitization
    return hostname.strip()
    #return "hostname.org"  # fixture

def ask_ip():
    #print("Asking the user for ip.")
    ip = input("Ip:" )
    # TODO: sanitize
    return ip.strip()
    #return "127.0.0.1"  # fixture

def add_to_dns(hostname, ip, dns):
    """ Adds hostname and ip to the dns."""
    #print("Adding", hostname, ip, "to", dns)
    dns[hostname] = ip
    return dns

def user_interact(dns):
    """TODO
    In a loop:
     - show the dictionary
     - ask for hostname and ip
     - you might stop the loop when no hostname
     - add to dns
     """

    while True:
        print_dns(dns)
        hostname = ask_hostname()
        if not hostname:
            break
        ip = ask_ip()
        try:
            dns = add_to_dns(hostname, ip, dns)
        except Exception:
            print("Couldn't add ", hostname, "to dns.")
        input("Continue")

    print("Goodbye.")


def main():
    print("Starting dns.")
    dns = { 'rjekker.nl': '136.144.169.117' }
    user_interact(dns)
    print("Ending the program.")


if __name__ == "__main__":
    main()