import sys

the_name = "Jack"  # global variables

""" API 
Aplication
Programming
Interface"""

def add(second, third, first=0):
    """
    
    :param second:
    :param third:
    :param first:
    :return:
    """


    total = first + second + third
    return total

def print_name(name):
    #name = "Jeroen"  # local variable, newly instantiated
    print("The name is", name)
    return None

def main():
    local_name = "Peter"  # local variable
    print_name(name=local_name)
    print(local_name)

    value_a = 4
    value_b = 5
    value_c = 6
    add(5, 8)  # lost information
    total = add(value_a, value_b, value_c)
    print("The total is", total)

main()

