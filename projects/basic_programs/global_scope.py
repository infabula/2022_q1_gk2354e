name = "Global Knowledge"
PI = 3.159
_private = "money"
__double_underscores__ = "Pyton specific"
__dunders__ = __double_underscores__

def arg_fun(name):
    print(name)

def closure(name):
    def inner_function():
        print(name)
    return inner_function

def fun():
    global name
    name = "Jeroen"  # intention: change the global name variable
    print(name)

def main():
    global PI
    PI = 42.0
    fun()
    print("Global name is: ", name)

main()