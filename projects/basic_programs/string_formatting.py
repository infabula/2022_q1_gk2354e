# special characters in strings

message = 'Don\'t walk on the grass.'
message = "Don't walk on the grass."

filepath = "c:\\tmp\\native\\file.txt"
print(filepath)
filepath = r"c:\tmp\native\file.txt"
print(filepath)

# formated string templating system
number = 42
temperature = 23.3
naive = "The number is " + str(number) + "."

formatted = "The number is {}.".format(number)
formatted = "The number is {num} and the temperature is {temp}.".format(num=number,
                                                                        temp=temperature)

print(formatted)
formatted = f"The number is {number} and the temperature is {temperature}."
first_name = "Guido"
last_name = "van Rossum"
age = 42
line = f"{first_name:<20}{last_name:^20}{age:>4}"
print(line)

