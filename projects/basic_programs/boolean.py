def is_day_time(hour):
    return (hour >= 9) and (hour < 18)


def main():
    hour = 14
    day = 'Tuesday'
    shop_is_open = is_day_time(hour) and not (day == 'sunday')

    if shop_is_open:
        print("Let's go shopping.")
    else:
        print("OK, let's have a tea.")


if __name__ == "__main__":
    main()