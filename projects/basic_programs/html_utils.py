print("my name is", __name__)

def indent(content, spaces=2):
    indented = ' ' * spaces + content
    return  indented


def as_html(content, tag_name):
    html_content = "<" + tag_name + ">" + content + "</" + tag_name + ">"
    return html_content

if __name__ == "__main__":
    non_indent = "foobar"
    print(indent(non_indent, 5))
