#from html_utils import indent
#from other_utils import indent as o_indent
#from html_utils import as_html
import html_utils as hu


print("The main file has the name", __name__)

def main():
    header = "Paris"
    paragraph = "A romantic city."
    sub_header = "Walks"
    html_header = hu.as_html(content=header, tag_name="h1")
    html_paragraph = hu.as_html(paragraph, "p")
    html_sub_header = hu.as_html(sub_header, 'h2')

    indented_header = hu.indent(html_header, spaces=2)
    print(indented_header)
    print(hu.indent(html_paragraph, spaces=4))
    print(hu.indent(html_sub_header, spaces=4))


if __name__ == "__main__":
    main()