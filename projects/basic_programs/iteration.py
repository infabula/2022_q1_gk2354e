def print_numbers():
    count = 0
    while count < 100:
        count += 1
        if count < 20:
            continue
        if count == 50:
            break
        print(count)


def range_stuff():
    for num in range(100, 117, 2):
        if num >= 110:
            break
        print(num)
    print("The last number was", num)

def main():
    #print_numbers()
    range_stuff()

main()