import json
import requests


def create_deck_of_cards(test=False):
    """
    create the url
    do a get request
    find the deck_id in the dictionary
    and return
    or raise exception if not available
    :arg test set to True to return a fixture
    """
    print("Creating a new deck of cards.")
    url = "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1"
    response = requests.get(url)
    if response.status_code == 200:
        data = json.loads(response.text)
        #print(data)
        return data['deck_id']

def store_deck_id(deck_id, deck_id_filename):
    """Open the file and write the deck_id"""
    print("Storing ", deck_id, "to file", deck_id_filename)

def read_deck_id(deck_id_filename):
    """reads the deck_id from file,
    or raises an exception if it can't read it"""
    print("reading the deck_id from file", deck_id_filename)
    return "xrogovwui0b5"  # fixture
    #raise ValueError('Could not read deck_id.')


def draw_cards(amount, deck_id):
    """Draws an amount of cards and returns them"""
    print("Draw", amount, "cards", "from", deck_id)
    url = f"https://deckofcardsapi.com/api/deck/{deck_id}/draw/?count={amount}"
    response = requests.get(url)
    if response.status_code == 200:
        data = json.loads(response.text)
        #print(data['cards'])
        return data['cards']
    """
    return [
        {
            "image": "https://deckofcardsapi.com/static/img/KH.png",
            "value": "KING",
            "suit": "HEARTS",
            "code": "KH"
        },
        {
            "image": "https://deckofcardsapi.com/static/img/8C.png",
            "value": "8",
            "suit": "CLUBS",
            "code": "8C"
        }
    ]
    """

def print_cards(cards):
    """pretty prints the cards list"""
    print("THE CARDS ")
    for index, card in enumerate(cards, 1):
        value = card['value'] #card.get('value' "UNKNOWN")
        suit = card.get('suit', "UNKNOWN")

        line = "#{index} {suit} {value}".format(index=index, suit=suit, value=value)
        print(line)


def main():
    """TODO implement"""
    print("Welcome to deck of cards.")
    """Wouldn't it be nice if...
    we could get a random new deck of cards from the REST API
    and store (persistence) the deck_id on file.
    Next draw 10 cards 
    and show them on the terminal.
    """
    deck_id_filename = 'deck_id.txt'
    try:
        deck_id = read_deck_id(deck_id_filename)
    except Exception:
        deck_id = create_deck_of_cards(test=True)
        print("We got a deck_id", deck_id)
        store_deck_id(deck_id, deck_id_filename)

    cards = draw_cards(amount=2, deck_id=deck_id)
    print_cards(cards)
    print("Goodbye")


if __name__ == "__main__":
    main()